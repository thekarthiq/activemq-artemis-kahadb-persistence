package com.activemq.demo.activemqjmsdemo;

import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.store.PersistenceAdapter;
import org.apache.activemq.store.kahadb.KahaDBPersistenceAdapter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.File;

@SpringBootApplication
public class ActivemqJmsDemoApplication {

	@Bean(initMethod = "start", destroyMethod = "stop")
	public BrokerService broker() throws Exception {
		final BrokerService broker = new BrokerService();
		//broker.addConnector("tcp://localhost:61616");
		broker.addConnector("vm://localhost");
		PersistenceAdapter persistenceAdapter = new KahaDBPersistenceAdapter();
		File dir = new File(System.getProperty("user.home") + File.separator + "kaha");
		if (!dir.exists()) {
			dir.mkdirs();
		}
		persistenceAdapter.setDirectory(dir);
		broker.setPersistenceAdapter(persistenceAdapter);
		broker.setPersistent(true);
		return broker;
	}


	public static void main(String[] args) {

		SpringApplication.run(ActivemqJmsDemoApplication.class, args);

	}

}
